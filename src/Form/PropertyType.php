<?php

namespace App\Form;

use App\Entity\Option;
use App\Entity\Property;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, ['label' => 'Titre'])
            ->add('description')
            ->add('size', null, ['label' => 'Surface'])
            ->add('nbPlace', null, ['label' => 'Nombre de place'])
            ->add('city', null, ['label' => 'Ville'])
            ->add('adress', null, ['label' => 'Adresse'])
            ->add('price', null, ['label' => 'Prix'])
            ->add('availability', null, ['label' => 'Disponibilité'])
            ->add('phoneNb', null, ['label' => 'Numéro de téléphone du locataire'])
            ->add('surface')
            ->add('pieces')
            ->add('reduction')
            ->add('options', EntityType::class, [
                'class' => Option::class,
                'required' => false,
                'choice_label' => 'name',
                'multiple' => true
            ])
            ->add('imageFile', FileType::class, [
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Property::class,
        ]);
    }
}
