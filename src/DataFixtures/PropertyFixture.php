<?php

namespace App\DataFixtures;

use App\Entity\Property;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class PropertyFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        for($i =0; $i<100; $i++){
            $property = new Property();
            $property
                ->setTitle($faker->words(3,true))
                ->setDescription($faker->sentence(3,true))
                ->setNote($faker->numberBetween(6,10))
                ->setNbPlace($faker->numberBetween(1,50))
                ->setCity($faker->city)
                ->setAdress($faker->address)
                ->setPrice($faker->numberBetween(100,10000))
                ->setAvailability(true)
                ->setPhoneNb($faker->numberBetween(100000000,999999999))
                ->setSurface($faker->numberBetween(20,500))
                ->setPieces($faker->numberBetween(1,10))
                ->setReduction($faker->numberBetween(0,100))
                ->setFileName('empty.jpg');
            $manager->persist($property);
        }
        $manager->flush();
    }
}
