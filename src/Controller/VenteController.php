<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Twig\Environment;
use Symfony\Component\Routing\Annotation\Route;


class VenteController extends AbstractController
{

    /**
     * @Route("/condition", name="condition")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(){

        return $this->render('home/vente.html.twig');

    }

}