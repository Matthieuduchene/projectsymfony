<?php
namespace App\Controller;

use App\Entity\Commentaires;
use App\Entity\Comments;
use App\Entity\Property;
use App\Entity\PropertySearch;
use App\Form\CommentsType;
use App\Form\PropertySearchType;
use App\Repository\PropertyRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;
use function Symfony\Component\Translation\t;

class PropertyController extends AbstractController {

    /**
     * @var PropertyRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(PropertyRepository $repository, EntityManagerInterface $em){

        $this->repository = $repository;
        $this->em = $em;

    }

    /**
     * @Route("/appartements", name="properties")
     */
    public function index(PaginatorInterface $paginator, Request $request){

        $search = new PropertySearch();
        $form = $this->createForm(PropertySearchType::class, $search);
        $form->handleRequest($request);

        $properties =  $paginator->paginate($this->repository->findAllVisibleQuery($search),
            $request->query->getInt('page', 1),
        15);
        return $this->render('property/index.html.twig', ['current_menu' => 'properties',
            'properties' => $properties,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/appartements/{slug}-{id}", name="property.show", requirements={"slug": "[a-z0-9\-]*"})
     * @param Property $property
     * @return Response
     */
    public function show(Property $property, string $slug, Request $request){

        $comment = new Comments();

        $commentForm = $this->createForm(CommentsType::class, $comment);

        $commentForm->handleRequest($request);

        if($commentForm->isSubmitted() && $commentForm->isValid()){

            if(!$this->isGranted('IS_AUTHENTICATED_FULLY')){

                $this->addFlash('warning','Veuillez vous connecter avant de poster un message.');
                return $this->render('property/show.html.twig', [
                    'property' => $property,
                    'current_menu' => 'properties',
                    'commentForm' => $commentForm->createView()
                ]);

            }

            $comment->setCreatedAt(new \DateTime());
            $comment->setProperty($property);

            $parentid = $commentForm->get("parentid")->getData();

            $em = $this->getDoctrine()->getManager();

            if ($parentid != null){
                $parent = $em->getRepository(Comments::class)->find($parentid);
            }

            $comment->setParent($parent ?? null);

            $em->persist($comment);
            $em->flush();

            $this->addFlash('message', 'Votre commentaire a bien été envoyé');
            return $this->redirectToRoute('property.show', [
                'id' => $property->getId(),
                'slug' => $property->getSlug()
            ]);
        }


        if ($property->getSlug() !== $slug){
            return $this->redirectToRoute('property.show', [
                'id' => $property->getId(),
                'slug' => $property->getSlug()
            ], 301);
        }
        return $this->render('property/show.html.twig', [
            'property' => $property,
            'current_menu' => 'properties',
            'commentForm' => $commentForm->createView()
        ]);
    }

}